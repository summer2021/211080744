import mars.tensor as mt
from mars.session import new_session


if __name__ == '__main__':
    a = mt.ones((5, 5), chunk_size=3)
    b = a * 4
    # if there isn't a local session,
    # execute will create a default one first
    b.execute()
    
    # or create a session explicitly
    sess = new_session()
    b.execute(session=sess)  # run b
    print('successfully running')
